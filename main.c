#define _DEFAULT_SOURCE

#include <assert.h>
#include <locale.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <wchar.h>

#ifdef __linux
#include <sys/ioctl.h>
#include <unistd.h>
#endif

// #define ASCII_CHARSET

typedef uint8_t u8;
typedef uint32_t u32;
typedef uint64_t u64;
typedef unsigned __int128 u128;
typedef double f64;

volatile sig_atomic_t running = true;

static const wchar_t charset[] = {
#ifndef ASCII_CHARSET
	0xff66, 0xff67, 0xff68, 0xff69, 0xff6a, 0xff6b, 0xff6c, 0xff6d, 0xff6e, 0xff6f, 0xff70, 0xff71,
	0xff72, 0xff73, 0xff74, 0xff75, 0xff76, 0xff77, 0xff78, 0xff79, 0xff7a, 0xff7b, 0xff7c, 0xff7d,
	0xff7e, 0xff7f, 0xff80, 0xff81, 0xff82, 0xff83, 0xff84, 0xff85, 0xff86, 0xff87, 0xff88, 0xff89,
	0xff8a, 0xff8b, 0xff8c, 0xff8d, 0xff8e, 0xff8f, 0xff90, 0xff91, 0xff92, 0xff93, 0xff94, 0xff95,
	0xff96, 0xff97, 0xff98, 0xff99, 0xff9a, 0xff9b, 0xff9c, 0xff9d
#else
	'!', '"', '#',  '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3',
	'4', '5', '6',  '7', '8', '9', ':',  ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F',
	'G', 'H', 'I',  'J', 'K', 'L', 'M',  'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y',
	'Z', '[', '\\', ']', '^', '_', '`',  'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
	'm', 'n', 'o',  'p', 'q', 'r', 's',  't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~'
#endif
};

uint64_t get_terminal_size(void) {
	uint64_t rows;
	uint64_t cols;

#ifdef __linux
	struct winsize w = {0};
	assert(ioctl(STDIN_FILENO, TIOCGWINSZ, &w) >= 0);
	rows = w.ws_row;
	cols = w.ws_col;
#else
	rows = 20;
	cols = 25;
#endif

	return rows << 32 | cols;
}

void sigint_handler(int x) {
	(void) x;
	running = false;
	signal(SIGINT, SIG_IGN);
}

/// Generate a random u64 based on the given seed using the WyHash algorithm.
u64 rng_u64(u64 seed) {
	seed += (u64) 0x60bee2bee120fc15ULL;
	u128 tmp = (u128) seed * (u128) 0xa3b195354a39b70dULL;
	u64 m1 = (u64) (tmp >> 64 ^ tmp);
	tmp = (u128) m1 * (u128) 0x1b03738712fad5c9ULL;
	u64 m2 = (u64) (tmp >> 64 ^ tmp);
	return m2;
}

/// Generate a random f64 in range [0, 1].
f64 rng_f64(u64 seed) {
	static const f64 u64_max = (f64) 0xffffffffffffffffULL;
	const u64 gen = rng_u64(seed);
	return (f64) gen / u64_max;
}

typedef struct {
	u32 generation;

	u32 width;
	u32 height;
} Zigma;

#define MAX_WIDTH 1024

static inline void zigma_set_dimension(Zigma* zigma, u32 width, u32 height) {
	if (width > MAX_WIDTH) {
		width = MAX_WIDTH;
	}

	zigma->width = width;
	zigma->height = height;
}

Zigma zigma_new(u32 width, u32 height) {
	Zigma z = {.generation = 10000};
	zigma_set_dimension(&z, width, height);
	return z;
}

void render(Zigma* zigma) {
	static const u32 MAX_LEN = 20;
	static const f64 PROBABILITY = 0.02;

	u32 start_ys[MAX_WIDTH] = {0};
	u32 length[MAX_WIDTH] = {0};

	// Make the characters bold.
	fputs("\x1B[1m"
		  "\x1B[48;2;0;0;0m",
		  stdout);

	for (u32 y = 0; y < zigma->height + MAX_LEN; ++y) {
		putchar('\n');

		for (u32 x = 0; x < zigma->width; ++x) {
			// The stream in this column is slowed down by `slowdown` times.
			// Set it to some constant value to make all the columns synchronous.
			const u32 slowdown = (u32) rng_u64(~(u64) x) % 3 + 1;

			const u32 real_x = x;
			const u32 real_y = zigma->generation / slowdown - y;

			const u32 lower = start_ys[x];
			const u32 upper = start_ys[x] + length[x];

			// If this real co-ordinate doesn't belong to any stream then try to start a new stream.
			if (real_y < lower || real_y >= upper) {
				const u64 seed = (u64) real_y * ~(u64) real_x ^ (u64) real_x;
				const f64 gen = rng_f64(seed);
				if (gen < PROBABILITY) {
					length[x] = (u32) (rng_u64(seed ^ rng_u64(seed)) % (u64) MAX_LEN + 1ULL);
					start_ys[x] = real_y - (length[x] - 1);
				}
			}

			// Don't render the hidden part of the screen.
			if (y < MAX_LEN) {
				continue;
			}

			const u64 screen_seed = (u64) y * ~(u64) x ^ start_ys[x];
			const wchar_t c =
				charset[rng_u64(screen_seed) % (sizeof(charset) / sizeof(charset[0]))];

			// Render a stream that is already running.
			if (real_y == start_ys[x]) {
				printf("\x1B[38;2;%d;%d;%dm"
					   "%lc",
					   255, 255, 255, c);
			} else if (real_y < start_ys[x] + length[x] && real_y >= start_ys[x]) {
				const u32 offset = real_y - start_ys[x];
				const u32 scaler = length[x] + 3;
				const u8 intensity = (u8) ((scaler - offset) * 255 / scaler);
				printf("\x1B[38;2;%d;%d;%dm"
					   "%lc",
					   0, intensity, 0, c);

				/*
				if (start_ys[x] % 3 == 0) {
					printf("\x1B[38;2;%d;%d;%dm" "%lc", 0, intensity, intensity, c);
				} else if (start_ys[x] % 3 == 1) {
					printf("\x1B[38;2;%d;%d;%dm" "%lc", intensity, intensity, 0, c);
				} else {
					printf("\x1B[38;2;%d;%d;%dm" "%lc", 0, intensity, 0, c);
				}
				*/
			} else {
				putchar(' ');
			}
		}
	}

	// Reset the terminal state after each frame.
	fputs("\x1B[0m", stdout);
	zigma->generation += 1;
}

int main(void) {
	assert(setlocale(LC_ALL, "") != NULL && "ERROR: failed to set locale");

// #define BUFFER_SIZE ((22 + 3) * 512 * 128 + 100)
#define BUFFER_SIZE (7 * 1024 * 1024)
	char stdout_buffer[BUFFER_SIZE];
	setvbuf(stdout, stdout_buffer, _IOFBF, BUFFER_SIZE);
#undef BUFFER_SIZE

	signal(SIGINT, sigint_handler);
	static const uint32_t sleep_time = 50000;

	Zigma z[1] = {zigma_new(0, 0)};

	while (running) {
		const u64 size = get_terminal_size();
		zigma_set_dimension(z, (u32) (size & 0xFFFFFFFF), (u32) (size >> 32));

		// Clear the screen and render the current frame.
		fputs("\x1B[2J", stdout);
		render(z);

		fflush(stdout);
		usleep((useconds_t) sleep_time);
	}

	// Clear the screen and reset the terminal state.
	fputs("\x1B[1;1H\x1B[2J\x1B[?25h\x1B[m", stdout);
	fflush(stdout);

	return 0;
}
