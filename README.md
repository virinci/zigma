# zigma (Zig + Matrix)
A procedural generation based cmatrix clone that allows for seamless resizing of the terminal window. It also supports asynchronous scroll mode.

## Instructions
```
$ gcc -Werror -Wall -Wextra -Wconversion -std=c17 -Ofast -Wno-pedantic main.c
$ ./a.out
```

## To-Do
- [ ] Re-write it in Zig 🙃.

## Similar Projects
- <https://github.com/iannisdezwart/MatrixRain>
- <https://github.com/MikuChan03/matrix-effect>
